// let numeros = [1,2,3,4];

// let dobro = numeros.map(function(item){
//     // console.log(item);
//     return item * 2;
// });

// console.log(dobro);


// // filter
// let idades = [22,25,14,19,15];
// let maiores = idades.filter(function(item){
//     return item >=18;
// });
// console.log(maiores);

// // reduce
// let soma = numeros.reduce(function(acumulador, item) {
//   // console.log(acumulador, item);
//   return acumulador+ item;
// });
// console.log(soma);


//EX 01
let numeros = [2,4,6,8,10,12,14];
let par = numeros.map(function(item){
      return item + 1;
 });
  
  //console.log(par);

  //EX 02

  let nomes = ["Maria", "jose", "pedro", "Maria"];
   let nome = nomes.filter(function(item){
    return item =="Maria";
 });
 console.log(nome);

 //EX 03
 let numero = [22,25,14,19,15];
 let format = numero.reduce(function(acumulador, item) {
 // console.log(acumulador, item);
  return acumulador+' - '+ item;
});
  //console.log(format);

  // EX04

  let animais = ['Baleia', 'Jacaré', 'Dinossauro', 'T-rex'];
   animais.forEach(function(iten) {
    console.log("O animal é  " + iten);
  }) 
  
